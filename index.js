
function App() {
    return (
      <div id="containerApp" className="container">
      <Jeu></Jeu>
      </div>
    );
  }
  
  class Jeu extends React.Component {
    constructor(props) {
      super(props);
      this.state = { perso:null,enemi:null};
  
    }
  
    selectPerso=(dataPerso)=>
    {
     this.setState({perso:dataPerso});
    }

    selectEnemy=(dataEnemy)=>
    {
      console.log(this)
     this.setState({enemi:dataEnemy});
    }
    render() {
      let dataPerso=[
        {id:1,nom:"ranger",img:"Ranger_Art.jpg",maxHP:100,attaque:2,maxDegat:5,defence:1,vitesse:3,lien:"http://encyclopedie.naheulbeuk.com/spip.php?article131",texte:"Physiquement quelconque, moyen dans sa stature mais tout de même charismatique, le Ranger sait captiver l’auditoire par son aura mystérieuse et l’éclat d’acier de ses yeux."},
        {id:2,nom:"nain",img:"Nain_Art.jpg",maxHP:125, attaque:2,maxDegat:4,defence:3,vitesse:2,lien:"http://encyclopedie.naheulbeuk.com/spip.php?article144",texte:"Fils de mineur depuis 32 générations, notre Nain est un digne représentant de son peuple. Il n’a pas connu grand-chose d’autre que le métal, la roche et l’or avant d’être poussé par un instinct vénal sur le sentier de l’aventure."}
      ]
      let dataEnemy=[
        {id:1,nom:"Rat zombie",img:"Rat_Art.jpg",maxHP:60,attaque:2,maxDegat:5,defence:2,vitesse:1,texte:"Si certaines créatures peuvent devenir naturellement des zombis, par simple envie de s’accrocher à un semblant de vie, ce n’est pas le cas du rat. Ce mammifère est en effet un des plus raisonnables du monde animal (loin devant l’humain, notamment), et sait d’instinct quand il est temps de laisser tomber et de mourir. Il n’y a donc pas de rats zombis naturels, et pendant longtemps, il n’y a pas eu de rats zombis nécromantiques non plus. Les rares mages assez fous pour tenter le coup s’y cassaient régulièrement les dents. Et puis un jour, on a vu la première zombification de rongeurs réussie. Les résultats sont pour le moins inhabituels dans le petit monde de la nécromancie."},
        {id:2,nom:"Golem de Fer",img:"Golem_Art.jpg",maxHP:180,attaque:3,maxDegat:4,defence:4,vitesse:0,texte:`Le golem de fer est une créature fabriquée à moitié magiquement et à moitié alchimiquement. Et à moitié mécaniquement aussi. Vous me direz, ça fait beaucoup de moitiés, mais c’est parce qu’il est très gros. `}
      ]
      if(this.state.perso&&this.state.enemi)
      {
        return(<Combat perso={this.state.perso} enemi={this.state.enemi}></Combat>)
      }
      else if(this.state.perso&&!this.state.enemi)
      {
        return<ListEnemy dataEnemy={dataEnemy} selectPerso={this.selectEnemy}></ListEnemy>
      }
      else{
        return(<ListPerso dataPerso={dataPerso} selectPerso={this.selectPerso}></ListPerso>);
      }
    
    }
  
  }
  
  class ListPerso extends React.Component{
    constructor(props) {
      super(props);
      this.state = {};
    }
  
    render(){
      let lesPerso= this.props.dataPerso.map((unPerso)=>{
        return <Perso key={unPerso.id} data={unPerso} selectPerso={this.props.selectPerso}></Perso> 
       })
      return(<div className="row d-flex justify-content-around my-4">
      {lesPerso}
    </div>);
    }
  }
  
  class Perso extends React.Component{
    constructor(props)
    {
      super(props);
      if(!this.props.HP)
      {
      this.state={HP:this.props.data.maxHP}
      }
      else{
        this.state={HP:this.props.HP}
      }
    }
  
    render(){
      let HP=this.props.HP ? this.props.HP : this.props.data.maxHP;
      let dataPerso=this.props.data;
      return (<div id={"perso"+dataPerso.id} className="card personage" style={{width: "20rem"}}>
      <img className="card-img-top img-fluid" src={"img/"+dataPerso.img} alt="Card image cap"/>
      <div className="card-body">
        <h5 className="card-title">{dataPerso.nom.toUpperCase()}</h5>
        <p className="card-text">{dataPerso.texte}</p>
      </div>
      <ul className="list-group list-group-flush">
        <li className="list-group-item">
        <div className="progress">
        <div className="progress-bar bg-danger" role="progressbar" style={{width:(HP/(dataPerso.maxHP/100))+"%"}} aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">{HP} </div>
        </div>
        </li>
        <li className="list-group-item d-flex justify-content-around"> <div>Attaque : {dataPerso.attaque}</div> <div>Dégat : {this.props.data.maxDegat}</div></li>
        <li className="list-group-item d-flex justify-content-around"><div>Defense : {dataPerso.defence}</div><div> Vitesse : {this.props.data.vitesse}</div></li>
      </ul>
      <div className="card-body">
        {this.props.selectPerso?<button className="btn btn-secondary d-flex justify-content-center my-2 col-12" onClick={()=>{this.props.selectPerso(dataPerso)}} value={dataPerso.id} role="button">Choisir</button>:""}
        {dataPerso.lien?<a className="btn btn-secondary d-flex justify-content-center my-2" target="_blank" href={dataPerso.lien}>Biographie</a>:""}
      </div>
    </div>)
    }
 
  }
  
  class ListEnemy extends React.Component{
    constructor(props)
    {
      super(props);
      this.state = {};
    }
    render(){
     
        let lesEnemy= this.props.dataEnemy.map((unPerso)=>{
          return <Perso key={unPerso.id} data={unPerso} selectPerso={this.props.selectPerso}></Perso> 
         })
        return(<div className="row d-flex justify-content-around my-4">
        {lesEnemy}
      </div>
      )
    }
  }

  class Combat extends React.Component{
    constructor(props)
    {
      super(props);
      this.state={heroHP:this.props.perso.maxHP,enemiHP:this.props.enemi.maxHP,attaqueHero:null,attaqueEnemi:null};
      this.Hero=<Perso data={this.props.perso} HP={this.state.heroHP}></Perso>;
      this.Enemi=<Perso data={this.props.enemi} HP={this.state.enemiHP}></Perso>;
    }
    componentDidMount=()=>{
      let intervalHero=setInterval(()=>{
        this.setState({attaqueHero:this.attaque(this.Hero,this.Enemi)});
        if(this.state.heroHP<=0||this.state.enemiHP<=0)
        {
          clearInterval(intervalHero);
          clearInterval(intervalEnemi);
        }
      },1000-(50*this.Hero.props.data.vitesse))
      let intervalEnemi=setInterval(()=>{
        this.setState({attaqueEnemi:this.attaque(this.Enemi,this.Hero)});
        if(this.state.heroHP<=0||this.state.enemiHP<=0)
        {
          clearInterval(intervalHero);
          clearInterval(intervalEnemi);
        }
      },1000-(100*this.Enemi.props.data.vitesse))
 
    }
    render=()=>{
      
      console.log("Hero HP :"+this.state.heroHP)
      console.log("Enemi HP :"+this.state.enemiHP)
      if(this.state.heroHP<=0)
      {
        return(<h2 className="text-danger fs-2">Le Gagnant est le {this.props.enemi.nom.toUpperCase()}</h2>)
      }
      else if(this.state.enemiHP<=0){
        return(<h2 className="text-success fs-2">Le Gagnant est le {this.props.perso.nom.toUpperCase()}</h2>)
      }
      return(<div className="d-flex justify-content-between">
        
        <Perso data={this.props.perso} HP={this.state.heroHP}></Perso>
        <div className="align-self-center">
       <div className=" align-self-center mb-4">Le {this.props.perso.nom.toUpperCase()} attaque le {this.props.enemi.nom.toUpperCase()} pour {this.state.attaqueHero} dégats</div>
       <div className="align-self-center mt-4">Le {this.props.enemi.nom.toUpperCase()} attaque le {this.props.perso.nom.toUpperCase()} pour {this.state.attaqueEnemi} dégats</div>
       </div>
        <Perso data={this.props.enemi} HP={this.state.enemiHP}></Perso>
      
      </div>)
    }

    lanceDe(nbrDe,maximum)
    {
      let resultat=0;
      for(let i=0;i<=nbrDe;i++)
      {
        resultat+=Math.floor(Math.random()*maximum);
      }
      if(resultat==nbrDe*maximum)
      {
        console.log("LANCE DE : Coup Critique")
        resultat=resultat*2;
      }
      console.log("LANCE DE : "+resultat);
      return resultat;
    }

    attaque(origine,cible)
    {
      let nbrDeAttaque=origine.props.data.attaque;
      let degatAttaque=origine.props.data.maxDegat;
      let nbrDeDefence=cible.props.data.defence;
      let lancerAttaque=this.lanceDe(nbrDeAttaque,degatAttaque);
      let lancerDefence=this.lanceDe(nbrDeDefence,4);
      if(lancerAttaque>lancerDefence)
      {
        console.log(cible);
        if(cible==this.Hero)
        {
          this.setState({heroHP:this.state.heroHP-(lancerAttaque-lancerDefence)})
          // this.Hero.props.HP=this.Hero.props.HP-(lancerAttaque-lancerDefence);
        }
        else {
          this.setState({enemiHP:this.state.enemiHP-(lancerAttaque-lancerDefence)})
          // this.Enemi.props.HP=this.Enemi.props.HP-(lancerAttaque-lancerDefence);
        }
        return lancerAttaque-lancerDefence;
      }
      return 0;
    }
  }
  
  ReactDOM.render(<App />, document.getElementById('root'))

  